//
//  ViewController.m
//  YMCSpectrograph
//
//  Created by Mikhail Baynov on 26/10/2016.
//  Copyright © 2016 Mikhail Baynov. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController


- (void)viewDidLoad
{
   [super viewDidLoad];
   self.view.backgroundColor = [UIColor blackColor];
   self.audioPlot.color = [UIColor colorWithRed: 0.2 green: 0.4 blue: 0.6 alpha: 1];
   
   self.audioPlot.plotType = EZPlotTypeBuffer;
   self.audioPlot.shouldFill = YES;
   self.audioPlot.shouldMirror = YES;
   self.audioPlot.shouldOptimizeForRealtimePlot = NO;
   

   self.audioPlot.waveformLayer.shadowOffset = CGSizeMake(0.0, 1.0);
   self.audioPlot.waveformLayer.shadowRadius = 0.0;
   self.audioPlot.waveformLayer.shadowColor = [UIColor colorWithRed: 0.069 green: 0.543 blue: 0.575 alpha: 1].CGColor;
   self.audioPlot.waveformLayer.shadowOpacity = 1.0;
   
   [self openFileWithFilePathURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"ghost-sound" ofType:@"wav"]]];
}




- (void)openFileWithFilePathURL:(NSURL*)filePathURL
{
   self.audioFile = [EZAudioFile audioFileWithURL:filePathURL];
   self.filePathLabel.text = filePathURL.lastPathComponent;
   
   self.audioPlot.plotType = EZPlotTypeBuffer;
   self.audioPlot.shouldFill = YES;
   self.audioPlot.shouldMirror = YES;
   
   __weak typeof (self) weakSelf = self;
   [self.audioFile getWaveformDataWithCompletionBlock:^(float **waveformData,
                                                        int length)
    {
       [weakSelf.audioPlot updateBuffer:waveformData[0]
                         withBufferSize:length];
    }];
}

@end
