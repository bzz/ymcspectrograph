//
//  ViewController.h
//  YMCSpectrograph
//
//  Created by Mikhail Baynov on 26/10/2016.
//  Copyright © 2016 Mikhail Baynov. All rights reserved.
//

#import <UIKit/UIKit.h>

#include "EZAudio.h"


@interface ViewController : UIViewController


@property (nonatomic,strong) EZAudioFile *audioFile;
@property (nonatomic,weak) IBOutlet EZAudioPlot *audioPlot;
@property (nonatomic,weak) IBOutlet UILabel *filePathLabel;

@end

